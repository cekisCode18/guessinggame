var numberOfSquares = 6;
var colors = [];
var winningColor;

var squares = document.querySelectorAll(".square");
var showWinningColor = document.querySelector("#showRGB");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var reset = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");

reset.addEventListener("click", function () {
    resetGame();
});

init();

function init() {
    setUpModeButtons();
    setUpSquares();
    resetGame();
}

function resetGame() {
    colors = generateRandomColors(numberOfSquares);
    winningColor = pickWinningColor();
    showWinningColor.innerHTML = winningColor.slice(3, winningColor.length);
    h1.style.backgroundColor = "steelblue";
    reset.innerHTML = "RESET COLORS";
    messageDisplay.innerHTML = "";
    for (var i = 0; i < squares.length; i++) {
        if (colors[i]) {
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];
        } else {
            squares[i].style.display = "none";
        }
    }
}

function setUpModeButtons() {
    for (var i = 0; i < modeButtons.length; i++) {
        modeButtons[i].addEventListener("click", function () {
            modeButtons[0].classList.remove("selected");
            modeButtons[1].classList.remove("selected");
            this.classList.add("selected");
            if (this.innerHTML === "EASY") {
                numberOfSquares = 3;
            } else {
                numberOfSquares = 6;
            }
            resetGame();
        });
    }
}

function setUpSquares() {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = colors[i];
        squares[i].addEventListener("click", function () {
            checkForWin(this);
        });
    }
}

function checkForWin(clickedSquare) {
    if (clickedSquare.style.backgroundColor === winningColor) {
        onWin(winningColor);
    } else {
        clickedSquare.style.backgroundColor = "#232323";
        messageDisplay.innerHTML = "Try Again";
    }
}

function onWin(color) {
    messageDisplay.innerHTML = "YOU WIN!";
    for (var i = 0; i < colors.length; i++) {
        squares[i].style.backgroundColor = color;
    }
    h1.style.backgroundColor = color;
    reset.innerHTML = "PLAY AGAIN?";
}

function pickWinningColor() {
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

function generateRandomColors(num) {
    var randomColors = [];
    for (var i = 1; i <= num; i++) {
        var getRed = Math.floor(Math.random() * 256);
        var getGreen = Math.floor(Math.random() * 256);
        var getBlue = Math.floor(Math.random() * 256);
        colorString = "rgb(" + getRed + ", " + getGreen + ", " + getBlue + ")";
        randomColors.push(colorString);
    }
    return randomColors;
}
